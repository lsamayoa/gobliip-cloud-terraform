# Use local etcd proxy to connect to etcd cluster
# Terraform sets k8s_network_range
KUBE_APISERVER_OPTS="--insecure-bind-address=0.0.0.0 \
 --etcd_servers=http://127.0.0.1:2379 \
 --service-cluster-ip-range=${k8s_network_range} \
 --logtostderr=true"

KUBE_CONTROLLER_MANAGER_OPTS="--master=${k8s_api_server} \
 --logtostderr=true"

KUBE_SCHEDULER_OPTS="--master=${k8s_api_server} \
 --logtostderr=true"

 KUBE_KUBELET_OPTS="--address=0.0.0.0 \
 --api_servers=${k8s_api_server} \
 --enable-server \
 --logtostderr=true"

 KUBE_PROXY_OPTS="--master=${k8s_api_server} \
 --logtostderr=true"
