#cloud-config

coreos:
  etcd2:
    # Discovery is populated by Terraform
    discovery: ${etcd_discovery_url}
    # $public_ipv4 and $private_ipv4 are populated by the cloud provider
    advertise-client-urls: http://$private_ipv4:2379
    initial-advertise-peer-urls: http://$private_ipv4:2380
    listen-client-urls: http://0.0.0.0:2379
    listen-peer-urls: http://$private_ipv4:2380
    proxy: on
  fleet:
      public-ip: $private_ipv4
      metadata: role=k8s-node
  flannel:
    interface: $private_ipv4
  units:
    - name: etcd2.service
      command: start
    - name: flanneld.service
      command: start
    - name: fleet.service
      command: start
    - name: docker.service
      command: start
