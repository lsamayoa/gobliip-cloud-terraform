resource "google_compute_network" "gobliip_k8s_network" {
  name = "gobliip-k8s-network"
  ipv4_range = "${var.CLUSTER_NETWORK_RANGE}"
}

resource "google_compute_firewall" "gobliip_k8_allow_internet_ssh_traffic" {
  name = "gobliip-k8-allow-internet-ssh-traffic"
  network = "${google_compute_network.gobliip_k8s_network.name}"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports = ["22"]
  }
}

resource "google_compute_firewall" "gobliip_k8_allow_all_internal_traffic" {
  name = "gobliip-k8-allow-all-internal-traffic"
  network = "${google_compute_network.gobliip_k8s_network.name}"
  source_ranges = ["${var.CLUSTER_NETWORK_RANGE}"]

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "udp"
    ports = ["1-65535"]
  }

  allow {
    protocol = "tcp"
    ports = ["1-65535"]
  }
}

variable "CLUSTER_NETWORK_RANGE" { }
