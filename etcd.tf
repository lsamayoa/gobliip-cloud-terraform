resource "google_compute_instance" "etcd" {
  depends_on = ["template_file.etcd_cloud_config", "google_compute_network.gobliip_k8s_network"]
  count = "${var.ETCD_COUNT}"
  name = "etcd-node-${count.index+1}-${var.ETCD_COUNT}"
  machine_type = "n1-standard-1"
  zone = "us-central1-a"

  disk {
    image = "coreos-alpha-794-0-0-v20150903"
  }

  metadata {
    cluster-size = "${var.ETCD_COUNT}"
    user-data = "${template_file.etcd_cloud_config.rendered}"
  }

  network_interface {
    network = "${google_compute_network.gobliip_k8s_network.name}"
    access_config {
      // Ephemeral IP
    }
  }
}

resource "template_file" "etcd_cloud_config" {
  depends_on = ["template_file.etcd_discovery_url"]
  filename = "etcd/etcd_cloud_config.yaml.tpl"
  vars {
    etcd_discovery_url = "${file(var.ETCD_DISCOVERY_URL)}"
  }
}

resource "template_file" "etcd_discovery_url" {
  filename = "/dev/null"
  provisioner "local-exec" {
    command = "curl https://discovery.etcd.io/new?size=${var.ETCD_COUNT} > ${var.ETCD_DISCOVERY_URL}"
  }
}

variable "ETCD_COUNT" { }

variable "ETCD_DISCOVERY_URL" {
  default = "etcd/etcd_discovery_url.txt"
}
