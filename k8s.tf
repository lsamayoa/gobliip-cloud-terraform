resource "template_file" "k8s_env" {
  depends_on = ["google_compute_network.gobliip_k8s_network"]
  filename = "k8s/kubernetes.env.tpl"
  vars {
    k8s_api_server = "http://k8s-master.c.${var.GCE_PROJECT}.internal:8080"
    k8s_network_range = "${google_compute_network.gobliip_k8s_network.ipv4_range}"
  }
}

resource "google_compute_instance" "k8s_master" {
  depends_on = ["google_compute_instance.etcd", "template_file.k8s_env"]
  name = "k8s-master"
  machine_type = "${var.K8S_MACHINE_TYPE}"
  zone = "${var.GCE_ZONE}"

  disk {
    image = "${var.K8S_IMAGE}"
  }

  metadata {
    sshKeys = "core: ${file("keys/id_rsa.pub")}"
    user-data = "${template_file.k8s_master_cloud_config.rendered}"
  }

  can_ip_forward = true
  network_interface {
    network = "${google_compute_network.gobliip_k8s_network.name}"
    access_config {
      // Ephemeral IP
    }
  }
  service_account {
    scopes = ["https://www.googleapis.com/auth/compute"]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo cat <<'EOF' > /tmp/kubernetes.env\n${template_file.k8s_env.rendered}\nEOF",
      "sudo mv /tmp/kubernetes.env /etc/kubernetes.env",
      "sudo systemctl enable kube-apiserver",
      "sudo systemctl enable kube-controller-manager",
      "sudo systemctl enable kube-scheduler",
      "sudo systemctl start kube-apiserver",
      "sudo systemctl start kube-controller-manager",
      "sudo systemctl start kube-scheduler"
    ]
    connection {
      user = "core"
      agent = true
    }
  }
}

resource "google_compute_instance" "k8s_node" {
  depends_on = ["google_compute_instance.k8s_master", "template_file.k8s_env"]
  count = "${var.K8S_NODE_COUNT}"
  name = "k8s-node-${count.index+1}"
  machine_type = "${var.K8S_MACHINE_TYPE}"
  zone = "${var.GCE_ZONE}"

  disk {
    image = "${var.K8S_IMAGE}"
  }

  metadata {
    sshKeys = "core: ${file("keys/id_rsa.pub")}"
    user-data = "${template_file.k8s_node_cloud_config.rendered}"
  }

  can_ip_forward = true
  network_interface {
    network = "${google_compute_network.gobliip_k8s_network.name}"
    access_config {
      // Ephemeral IP
    }
  }
  service_account {
    scopes = ["https://www.googleapis.com/auth/compute"]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo cat <<'EOF' > /tmp/kubernetes.env\n${template_file.k8s_env.rendered}\nEOF",
      "sudo mv /tmp/kubernetes.env /etc/kubernetes.env",
      "sudo systemctl enable kube-kubelet",
      "sudo systemctl enable kube-proxy",
      "sudo systemctl start kube-kubelet",
      "sudo systemctl start kube-proxy"
    ]
    connection {
      user = "core"
      agent = true
    }
  }
}

resource "template_file" "k8s_node_cloud_config" {
  depends_on = ["template_file.etcd_discovery_url", "google_compute_network.gobliip_k8s_network"]
  filename = "k8s/k8s_node_cloud_config.yaml.tpl"
  vars {
    etcd_discovery_url = "${file(var.ETCD_DISCOVERY_URL)}"
  }
}

resource "template_file" "k8s_master_cloud_config" {
  depends_on = ["template_file.etcd_discovery_url", "google_compute_network.gobliip_k8s_network"]
  filename = "k8s/k8s_master_cloud_config.yaml.tpl"
  vars {
    etcd_discovery_url = "${file(var.ETCD_DISCOVERY_URL)}"
    flannel_network_range = "${var.FLANNEL_NETWORK_RANGE}"
  }
}

variable "FLANNEL_NETWORK_RANGE" {}

variable "K8S_NODE_COUNT" {}

variable "K8S_MACHINE_TYPE" {}

variable "K8S_IMAGE" {
  default = "k8s-v1-0-4-rc-coreos-794-0-0-v20150908-2"
}
