provider "google" {
  account_file = "${file("~/.gce/credentials/gobliip-cloud-d2153912a1a5.json")}"
  project = "${var.GCE_PROJECT}"
  region = "${var.GCE_REGION}"
}

variable "GCE_PROJECT" {}
variable "GCE_REGION" {}
variable "GCE_ZONE" {}
